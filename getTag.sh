#!/bin/bash

# Fetch and list the existing git tags
git fetch --tags
tags=$(git tag -l)

major=1
minor=0
version="$major.$minor"
for tag in $tags; do
   if [[ $tag =~ ^v([0-9]+)\.([0-9]+)$ ]]; then
   	    major_tmp=${BASH_REMATCH[1]}
        minor_tmp=${BASH_REMATCH[2]}
        if [[ $major_tmp -gt $major  ||  ( $major_tmp -eq $major  && $minor_tmp -gt $minor ) ]] ; then
     		major=$major_tmp
            minor=$minor_tmp
        fi
   fi
done

# Increment the minor version by 0.1
minor=$(bc <<< "$minor + 0.1")

# Format the new version number
version="$major.$minor"