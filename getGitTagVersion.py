#!/usr/bin/env python3

import subprocess
import re


TAG_PREFIX = "v"  # Prefix for Git tags
VERSION_REGEX = r"(\d+)\.(\d+)"  # Regular expression for matching major and minor version numbers
#REPO_URL = "https://gitlab.com/mses894/maaayanprotfolioapplication.git"  # URL of your Git repository

# Get the latest Git tag
current_tag = subprocess.check_output(["git", "describe", "--tags", "--abbrev=0"]).decode().strip()

# Extract the major and minor version numbers from the latest Git tag
match = re.search(VERSION_REGEX, current_tag)
current_version = f"{match.group(1)}.{match.group(2)}" if match else "1.0"

# Increment the minor version number by one
next_minor_version = int(match.group(2)) + 1
next_version = f"{match.group(1)}.{next_minor_version}"

# Generate the new Git tag and push it to the remote Git repository
new_tag = f"{TAG_PREFIX}{next_version}"
print(current_version)
print(new_tag)
subprocess.run(["git", "tag", new_tag])
subprocess.run(["git", "push", 'origin', new])