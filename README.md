***

## Pipeline for Todo App CI/CD

***

This is a Jenkins pipeline script for continuous integration and deployment of the Todo App. The pipeline has several stages that build, test, publish, and deploy the application using Docker containers and AWS services.




***Prerequisites***

Jenkins server with Pipeline plugin installed

Docker and Docker Compose installed on the Jenkins agent

AWS credentials with permissions to access ECR and deploy the application

SSH key for GitLab and GitOps repository access

Pipeline Steps

Increment Version

This stage runs only for the master branch and increments the minor version number of the application based on the latest Git tag. The new version number is stored in the environment variable VERSION and a new Git tag is created for the release.

  

***Pipeline Configuration***

The pipeline uses several environment variables to store the configuration parameters, such as the application name, AWS region, ECR URI, GitLab and GitOps repository URLs, etc. These variables can be modified to adapt the pipeline to different environments.

  

The pipeline also requires credentials for AWS and GitLab access, which should be added to the Jenkins credential store and referenced by their IDs in the pipeline script.

  

***Build image***

This stage builds the Docker image of the Todo App, using the version number from the previous stage or the Git commit hash for other branches. The image is tagged with both the version number and the latest tag (for the master branch) or the commit hash (for other branches).

  

***Local Tests***

This stage runs the integration tests of the Todo App locally using Docker Compose and MongoDB. It starts the required services, runs the tests, and stops the services.

  

***Git push tag***

This stage pushes the newly created Git tag to the GitLab repository for release management.

  

***Publish***

This stage publishes the Docker image to the AWS Elastic Container Registry (ECR) using the AWS CLI. It logs in to ECR, tags the image with the ECR URI and the version number, and pushes the image to ECR.

  

***Deploy***

This stage deploys the Todo App to a Kubernetes cluster using GitOps methodology. It clones the GitOps repository, updates the image tag in the values.yaml file with the new version number, commits and pushes the change to the GitOps repository. The deployment is managed by a ArgoCD/Kubernetes controller watching the GitOps repository changes.

Diagram A: Cloud infrastructure
 ![alt text](https://github.com/mses894/myimages/blob/main/aws%20cloud%20diagram.PNG?raw=true)
 
Diagram A: CICD app diagram
![alt text](https://github.com/mses894/myimages/blob/main/CICD%20app%20diagram.PNG?raw=true)


***Author***

This pipeline script was created by Maayans for the Todo App project. Please feel free to contact me for any questions or suggestions.
