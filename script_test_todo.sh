#!/bin/bash

URL=$1

# Test /api route

echo '''
___________        .___   ______________________ ________________________ 
\__    ___/___   __| _/___\__    ___/\_   _____//   _____/\__    ___/_   |
  |    | /  _ \ / __ |/  _ \|    |    |    __)_ \_____  \   |    |   |   |
  |    |(  <_> ) /_/ (  <_> )    |    |        \/        \  |    |   |   |
  |____| \____/\____ |\____/|____|   /_______  /_______  /  |____|   |___|
                    \/                       \/        \/                 
          '''



echo "TEST 1 - Testing /api route"
EXPECTED_OUTPUT='[]'
ACTUAL_OUTPUT=$(curl -s -X GET $URL/api)
if [ "$EXPECTED_OUTPUT" != "$ACTUAL_OUTPUT" ]; then
  echo "Error: /api output did not match expected output"
  exit 1
fi
echo "Success: /api output matched expected output"
echo ""

# Test /api/add route
echo '''

___________        .___   ______________________ ____________________________  
\__    ___/___   __| _/___\__    ___/\_   _____//   _____/\__    ___/\_____  \ 
  |    | /  _ \ / __ |/  _ \|    |    |    __)_ \_____  \   |    |    /  ____/ 
  |    |(  <_> ) /_/ (  <_> )    |    |        \/        \  |    |   /       \ 
  |____| \____/\____ |\____/|____|   /_______  /_______  /  |____|   \_______ \
                    \/                       \/        \/                    \/

                   '''


echo "TEST 2 - Testing /api/add route"
EXPECTED_OUTPUT='{"task": "Buy groceries"}'
ACTUAL_OUTPUT=$(curl -s -X POST $URL/api/add -d "task=Buy groceries")
if [ $EXPECTED_OUTPUT != $ACTUAL_OUTPUT ]; then
  echo "Error: /api/add output did not match expected output"
  exit 1
fi
echo "Success: /api/add output matched expected output"
echo ""

# Test /api/edit route
echo '''

___________        .___   ______________________ ____________________________  
\__    ___/___   __| _/___\__    ___/\_   _____//   _____/\__    ___/\_____  \ 
  |    | /  _ \ / __ |/  _ \|    |    |    __)_ \_____  \   |    |     _(__  < 
  |    |(  <_> ) /_/ (  <_> )    |    |        \/        \  |    |    /       \
  |____| \____/\____ |\____/|____|   /_______  /_______  /  |____|   /______  /
                    \/                       \/        \/                   \/ 

'''


echo "TEST 3 - Testing /api/edit route"
EXPECTED_OUTPUT='{"new_task":"Buy milk","old_task":"Buy groceries"}'
ACTUAL_OUTPUT=$(curl -s -X PUT $URL/api/edit -d "old_task=Buy groceries&new_task=Buy milk")
if [ $EXPECTED_OUTPUT != $ACTUAL_OUTPUT ]; then
  echo "Error: /api/edit output did not match expected output"
  exit 1
fi
echo "Success: /api/edit output matched expected output"
echo ""

# Test /api/delete route

echo '''

___________        .___   ______________________ _________________________  
\__    ___/___   __| _/___\__    ___/\_   _____//   _____/\__    ___/  |  | 
  |    | /  _ \ / __ |/  _ \|    |    |    __)_ \_____  \   |    | /   |  |_
  |    |(  <_> ) /_/ (  <_> )    |    |        \/        \  |    |/    ^   /
  |____| \____/\____ |\____/|____|   /_______  /_______  /  |____|\____   | 
                    \/                       \/        \/              |__| 

'''
echo "TEST 4 - Testing /api/delete route"
EXPECTED_OUTPUT='{"task":"Buy milk"}'
ACTUAL_OUTPUT=$(curl -s -X POST $URL/api/delete -d "task=Buy milk")
if [ $EXPECTED_OUTPUT != $ACTUAL_OUTPUT ]; then
  echo "Error: /api/delete output did not match expected output"
  exit 1
fi
echo "Success: /api/delete output matched expected output"
echo ""