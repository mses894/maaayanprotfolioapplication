pipeline {
    agent any
    environment { 
        APP_NAME = 'todo'
        AWS_REGION = 'eu-west-1'
        ECR_URI = '644435390668.dkr.ecr.eu-west-1.amazonaws.com/maayan-ecr'    
        GITLAB_USERNAME = 'mses894' 
        GITLAB_SSH_URI = 'git@gitlab.com:mses894/maaayanprotfolioapplication.git'  

        GITOPS_REPO = 'git@gitlab.com:mses894/maaayanprotfoliogitops.git'
        GIT_BRANCH = 'master'
    }
    stages {
        stage('Increment Version') {
            when {
                branch 'master'
            }
            steps {
                script {
                    def tagList = sh(returnStdout: true, script: 'git tag  --list --sort=-v:refname').trim().split('\n')
                    echo "Existing Git Tags: ${tagList}"
                // Find the most advanced git tag and extract the minor version number
                    def lastTag = tagList[0]
                    echo "lastTag $lastTag"
                    currentVersion = lastTag.replaceAll('v', '')
                    env.CURRENTVERSION = currentVersion
                    echo "Current Version: ${currentVersion}"
                // Increment the minor version number
                    def parts = currentVersion.tokenize('.')
                    def major = parts[0].toInteger()
                    def patch = parts[1].toInteger()
                    patch += 1
                    echo "patch = ${patch}"
                // Construct the new version string
                    def newVersion = "${major}.${patch}"
                    echo "Next Version: ${newVersion}"
                // Set the version number as an environment variable for use in following stages
                    env.VERSION = newVersion
                    echo "VERSION=${env.VERSION}"

                    sh "git tag v${env.VERSION}"
                }
            }
        }
        stage('Build image') {
            steps {  
                script {    
                    if (env.BRANCH_NAME == "master") {
                        sh "docker build -t ${env.APP_NAME}:${env.VERSION} -t ${env.APP_NAME}:latest ."
                    }
                    else {
                        def commitHash = sh(returnStdout: true, script: 'git rev-parse --short HEAD').trim()
                        sh "docker build -t ${env.APP_NAME}:${commitHash} ."
                    }
                }
            }
        }
        stage('Local Tests') { 
            steps {
                script {
                    sh 'docker-compose -f docker-compose-test.yaml up mongodb-maayan -d'
                    sh 'docker-compose -f docker-compose-test.yaml up app -d'
                    sh 'docker-compose -f docker-compose-test.yaml up test'
                    sh 'docker-compose -f docker-compose-test.yaml down'
                }
            }
        }
        stage('Git push tag') {
          steps {
                sh "git config user.name ${env.GITLAB_USERNAME}"
                sh "git config user.email ${env.GITLAB_USERNAME}"
                sshagent(['maayan-ssh-gitlab']) {
                    sh("""
                        #!/usr/bin/env bash
                        set +x
                        export GIT_SSH_COMMAND="ssh -oStrictHostKeyChecking=no"
                        git push ${env.GITLAB_SSH_URI} v${env.VERSION}
                     """)
                }
            }
        }
        stage('Publish') {
            steps {                
                echo "Publishing....${env.BRANCH_NAME}" 
                withAWS(credentials:'maayanawscredentIals', region:"${env.AWS_REGION}") {
                    sh "aws ecr get-login-password --region ${env.AWS_REGION} | docker login --username AWS --password-stdin ${env.ECR_URI}"                                        
                    // sh "docker tag ${env.APP_NAME}:${env.GIT_TAG_VERSION} ${env.ECR_URI}/${env.APP_NAME}:${env.GIT_TAG_VERSION}"
                    sh "docker tag ${env.APP_NAME}:${env.VERSION} ${env.ECR_URI}:${env.APP_NAME}-${env.VERSION}"
                    sh "docker push ${env.ECR_URI}:${env.APP_NAME}-${env.VERSION}"                        
                }
            }
        }
        stage('Deploy') {
         steps {
            sshagent(['maayan-ssh-gitlab']) {
               sh """
                  rm -fr maaayanprotfoliogitops
                  git clone ${env.GITOPS_REPO}
                  cd maaayanprotfoliogitops
                  sed -i 's/tag: ${env.APP_NAME}-.*\$/tag: ${env.APP_NAME}-${env.VERSION}/g' ./todoapp/values.yaml
                  git add ./todoapp/values.yaml
                  git commit -m "Update image tag in ${env.VERSION} for values.yaml"
                  git push ${env.GITOPS_REPO}
                """
                }
            }
        }      
    }
}